import Flutter
import UIKit
import UnityAds

public class UnityAdsLoadHandler :NSObject , UnityAdsLoadDelegate {
    public func unityAdsAdLoaded(_ placementId: String) {
    
    }
    
    public func unityAdsAdFailed(toLoad placementId: String) {
        var arguments = [String:Any]();
        arguments["error"] = 10
        arguments["message"] = placementId;
        SwiftUnityAdsFlutterPlugin.channel.invokeMethod("onUnityAdsError",arguments: arguments);

    }
    
    var result : FlutterResult?
    
    init(_ result: FlutterResult?) {
        self.result = result
    }
}
public class UnityAdsInitHandler :NSObject , UnityAdsInitializationDelegate {
    public func initializationComplete() {
        result(nil)
        UnityAds.add(delegate)
    }
    
    public func initializationFailed(_ error: UnityAdsInitializationError, withMessage message: String) {
        result(FlutterError(code: "10", message: "init failed unity ads", details: nil))
    }
    
   
    var result : FlutterResult
    var delegate: SwiftUnityAdsFlutterPlugin
    
    init(_ result: @escaping FlutterResult, _ delegate : SwiftUnityAdsFlutterPlugin) {
        self.result = result
        self.delegate = delegate
    }
}

public class SwiftUnityAdsFlutterPlugin: NSObject, FlutterPlugin, UnityAdsDelegate {
    
  
  public static  var channel = FlutterMethodChannel()
  public static func register(with registrar: FlutterPluginRegistrar) {
    channel = FlutterMethodChannel(name: "unity_ads_flutter", binaryMessenger: registrar.messenger())
    let instance = SwiftUnityAdsFlutterPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    //load show isReady initialize
    //ad ststuses
    if call.method == "initialize" {

      let gameId: String? = (call.arguments as? [String: Any])?["gameId"] as? String
      let testMode: Bool? = (call.arguments as? [String: Any])?["testMode"] as? Bool
      UnityAds.initialize(gameId! , testMode : testMode!, enablePerPlacementLoad : true,
                          initializationDelegate: UnityAdsInitHandler(result, self));

    } else if call.method == "load" {
        let placementId: String? = (call.arguments as? [String: Any])?["placementId"] as? String
        UnityAds.load(placementId!, loadDelegate: UnityAdsLoadHandler(result))
    } else if call.method == "isReady" {
        let placementId: String? = (call.arguments as? [String: Any])?["placementId"] as? String
        result(UnityAds.isReady(placementId!))
    } else if call.method == "show" {
        let placementId: String? = (call.arguments as? [String: Any])?["placementId"] as? String
        UnityAds.show((UIApplication.shared.delegate?.window??.rootViewController)!, placementId: placementId!)
        result(nil)
    } else {
      result("iOS " + UIDevice.current.systemVersion)
    }
  }

    public func unityAdsReady(_ placementId : String) {
        SwiftUnityAdsFlutterPlugin.channel.invokeMethod("onUnityAdsReady",arguments: placementId);
  }

    public func unityAdsDidStart(_ placementId : String) {
        SwiftUnityAdsFlutterPlugin.channel.invokeMethod("onUnityAdsStart",arguments: placementId);
  }
    
    public func unityAdsDidFinish(_ placementId: String, with state: UnityAdsFinishState) {
        var arguments = [String:Any]();
        arguments["placementId"]=placementId;
        arguments["result"]=state.rawValue;
        SwiftUnityAdsFlutterPlugin.channel.invokeMethod("onUnityAdsFinish",arguments: arguments);
    }
    
    

  

 
    public func  unityAdsDidError(_ error : UnityAdsError, withMessage message : String) {
    var arguments = [String:Any]();
    arguments["error"] = error.rawValue;
    arguments["message"] = message;
        SwiftUnityAdsFlutterPlugin.channel.invokeMethod("onUnityAdsError",arguments: arguments);
  }
}
