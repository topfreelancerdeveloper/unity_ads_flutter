package com.topfreelancerdeveloper.unity_ads_flutter;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;

import com.unity3d.ads.IUnityAdsInitializationListener;
import com.unity3d.ads.IUnityAdsListener;
import com.unity3d.ads.IUnityAdsLoadListener;
import com.unity3d.ads.UnityAds;

import java.util.HashMap;
import java.util.Map;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/** UnityAdsFlutterPlugin */
public class UnityAdsFlutterPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware, IUnityAdsListener {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private MethodChannel channel;
  private Activity activity;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "unity_ads_flutter");
    channel.setMethodCallHandler(this);
  }

  @Override
  public void onMethodCall(@NonNull final MethodCall call, @NonNull final Result result) {
    if (call.method.equals("getDebugMode")) {
      result.success(UnityAds.getDebugMode());
    } else if(call.method.equals("getDefaultPlacementState")) {
      result.success(UnityAds.getPlacementState().ordinal());
    } else if(call.method.equals("getPlacementState") && call.hasArgument("placementId")) {
      result.success(UnityAds.getPlacementState(call.<String>argument("placementId")).ordinal());
    } else if(call.method.equals("getVersion")){
      result.success(UnityAds.getVersion());
    } else if(call.method.equals("initialize") && call.hasArgument("gameId") && call.hasArgument("testMode")){
      UnityAds.initialize((Context) activity, call.<String>argument("gameId"), call.<Boolean>argument("testMode"), true, new IUnityAdsInitializationListener() {
        @Override
        public void onInitializationComplete() {
          result.success(null);
          UnityAds.addListener(UnityAdsFlutterPlugin.this);
        }

        @Override
        public void onInitializationFailed(UnityAds.UnityAdsInitializationError unityAdsInitializationError, String s) {
          result.error("10" , "init failed unity ads", null);
        }
      });


    } else if(call.method.equals("isInitialized")){
      result.success(UnityAds.isInitialized());
    } else if(call.method.equals("isDefaultReady")){
      result.success(UnityAds.isReady());
    } else if(call.method.equals("isReady") && call.hasArgument("placementId")){
      result.success(UnityAds.isReady(call.<String>argument("placementId")));
    } else if(call.method.equals("isSupported")){
      result.success(UnityAds.isSupported());
    } else if(call.method.equals("setDebugMode") && call.hasArgument("debugMode")){
      UnityAds.setDebugMode(call.<Boolean>argument("debugMode"));
      result.success(null);
    } else if(call.method.equals("showDefault")){
      UnityAds.show(activity);
      result.success(null);
    } else if(call.method.equals("show") && call.hasArgument("placementId")){
      UnityAds.show(activity,call.<String>argument("placementId"));
      result.success(null);
    } else if(call.method.equals("load") && call.hasArgument("placementId")){
      UnityAds.load(call.<String>argument("placementId"), new IUnityAdsLoadListener() {
        @Override
        public void onUnityAdsAdLoaded(String s) {
        }

        @Override
        public void onUnityAdsFailedToLoad(String s) {
          Map<String,Object> arguments=new HashMap<String,Object>();
          arguments.put("error",UnityAds.UnityAdsError.values().length);
          arguments.put("message",call.<String>argument("placementId"));
          channel.invokeMethod("onUnityAdsError",arguments);
        }
      });
      result.success(null);
    } else {
      result.notImplemented();
    }
  }

  @Override
  public void onUnityAdsReady(String placementId) {
    channel.invokeMethod("onUnityAdsReady",placementId);
  }

  @Override
  public void onUnityAdsStart(String placementId) {
    channel.invokeMethod("onUnityAdsStart",placementId);
  }

  @Override
  public void onUnityAdsFinish(String placementId, UnityAds.FinishState result) {
    Map<String,Object> arguments=new HashMap<String,Object>();
    arguments.put("placementId",placementId);
    arguments.put("result",result.ordinal());
    channel.invokeMethod("onUnityAdsFinish",arguments);
  }

  @Override
  public void onUnityAdsError(UnityAds.UnityAdsError error, String message) {
    Map<String,Object> arguments=new HashMap<String,Object>();
    arguments.put("error",error.ordinal());
    arguments.put("message",message);
    channel.invokeMethod("onUnityAdsError",arguments);
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
  }

  @Override
  public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
    activity = binding.getActivity();
  }

  @Override
  public void onDetachedFromActivityForConfigChanges() {
    activity = null;
  }

  @Override
  public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {
    activity = binding.getActivity();
  }

  @Override
  public void onDetachedFromActivity() {
    activity = null;
  }


}
